+++
title = "About me"
date = 2024-01-30
updated = 2024-01-30
description = "A brief intro about me ；）"
[taxonomies]
tags = ["hello", "zola", "getting-started"]
+++

My joy in Data Science is discovering unique insights for a company and understanding the purpose of data analysis in terms of requirements. Creating original work and collaborating with co-works bring me a great sense of achievement. I am a team player, I always try to put the group's accomplishment at the highest priority and handle tasks that benefit the whole team. 
Right now, I’m an Electrical and Computer Engineering graduate student at Duke University pursuing further study in data analysis and machine learning. With a computer science background, I interned in software engineering and data analysis and have grown my skills in multiple programming languages.


![the Duke University image](../duke.jpg)

[^zolawebsite]: Website: [zola](https://getzola.org), GitHub: [getzola/zola](https://github.com/getzola/zola)